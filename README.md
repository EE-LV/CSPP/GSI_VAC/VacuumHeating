﻿README
======
This is the LabVIEW AF/CS++ based project for the GSI Vacuum Heating System.

Currently used development SW is LabVIEW 2021 SP1

Related documents and information
=================================
- README.md
- Release_Notes.md
- EUPL v.1.1 - Lizenz.pdf
- Contact: H.Brand@gsi.de or G.Savino@gsi.de
- Download, bug reports... : https://git.gsi.de/EE-LV/CSPP/GSI_VAC/VacuumHeating
- Documentation:
  - Refer to Documantation Folder 
  - NI Actor Framework: https://decibel.ni.com/content/groups/actor-framework-2011?view=overview
  - CS Framework: http://wiki.gsi.de/cgi-bin/view/CSframework/WebHome

GIT Submodules
==============
- Packages/CSPP_Core: This package is used as submodule.
- Packages/CSPP_ObjectManager: This package is used as submodule.
- Packages/CSPP_MQTT: Communicatin layer based on MQTT
- Packages/CSPP_RT: Providing a librarie supporting LabVIEW-RT features. 
- Packages/CSPP_Syslog: Providing a Syslog based Message Handler 
- Packages/CSPP_Utilities: Providing some usefull utility classes. 
- instr.lib/Leybold23xxxx: Leybold CENTER TWO/THREE LabVIEW instrument driver.
- instr.lib/Leybold23xxx2: Leybold CENTER One LabVIEW instrument driver.
 

Run to pull:
  - git submodule init
  - git submodule update

External Dependencies
=================================
- Syslog; Refer to http://sine.ni.com/nips/cds/view/p/lang/de/nid/209116
- Monitored Actor; Refer to https://decibel.ni.com/content/thread/18301 and http://lavag.org/topic/17056-monitoring-actors
  
Getting started:
=================================
- Clone this repository, if not alread done.
- Switch to the desired branch.
- Get submodules:
  - git submodule init
  - git submodule update
- Create a hard link to the custom error file(s): 
  - cd <LabVIEW 2015>\user.lib\errors
  - mklink /h CSPP_Core-errors.txt Packages\CSPP_Core\CSPP_Core-errors.txt
- Create a project specific copy of "CSPP.lvproj"
  - or add CSPP_CoreContent.vi into your own LabVIEW project. You can drag the desired libraries from the dependencies into your virtual project folder structure.
- You need to create your project specific CSPP.ini-file, like "CSPP_Core.ini" containing samples of all classes and actors, etc.
- You need to create and deploy your project specific shared variable libraries.
  - Sample shared variable libraries should be available on disk in the corresponding package folder.
- Run your project specific "CS++Main.vi"
  - Concatenate the desired Content.vi's, at least _CSPP__CoreContent.vi_ , to the _Launch CSPP__StartActor.vi_ before execution.

Known issues:
=============
- Actor-Frontpanel-Web-Publishing is not supported in the LabVIEW development environment, but from an executable. You may need to change the WebServer-Listener port in niwebserver.conf, e.g. Listen 8082.
Be aware that VIs with '+' in their fully qualified name are not supported for unknown reason (LabVIEW-Bug?).
- Enhaunced DSC Support enabled will crash any LabVIEW 2021 SP1 executable. This is a bug accepted by NI.
- MQTT Qos=0 is recommended at this time to avoid protocol errors and performance problems.

Author: H.Brand@gsi.de, G.Savino@gsi.de

Copyright 2017 - 2022 GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.