Release Notes for the GSI Vacuum Heating System - 07-Nov-2022
=============================================================
This is the LabVIEW AF/CS++ based project for the GSI Vacuum Heating System.

Version 2.1.1.1
===============
Probably final version with LabVIEW 2021 SP1.

Version 2.1.1.0
===============
Use MQTT for network communication.

Version 1.4.2.0
===============
Correct color coding, improved alarm handling.

Version 1.4.1.0
===============
Publish SV Forced; Reser Forced in SubVI called in Polling Core.vi

Version 1.4.0.0
===============
Add Force-Flag to force power if temerature or different to ste-point is out of limits.

Version 1.3.2.2
===============
Release based on LV2021 SP1.

Version 0.1.0.0
===============
The is the first release based on LV2018.

Version 0.0.0.0
===============
The is the original version based on standard LV-RT.

